import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  'clientId': '03e461f2-58da-4bfa-b242-43b335f3e8e1',
  'redirectUri': 'http://localhost:4200',
  'postLogoutRedirectUri': '',
  'loginUrl': 'https://adfs2016.aareon.nl/adfs/oauth2/authorize/',
  'scope': 'openid profile email',
  'resource': '',
  'rngUrl': '',
  'oidc': true,
  'requestAccessToken': true,
  'options': null,
  'issuer': 'https://adfs2016.aareon.nl/adfs',
  'clearHashAfterLogin': true,
  'tokenEndpoint': 'https://adfs2016.aareon.nl/adfs/oauth2/token/',
  'userinfoEndpoint': 'https://adfs2016.aareon.nl/adfs/userinfo',
  'responseType': 'token',
  'showDebugInformation': true,
  'dummyClientSecret': null,
  'requireHttps': 'remoteOnly',
  'strictDiscoveryDocumentValidation': false,
  'jwks': {
    'keys': [
      {
        'kty': 'RSA',
        'alg': 'RS256',
        'use': 'sig',
        'kid': 'eriNKpXiNwVK23iz_bDWPycMPM4',
        'n':
            'pkEoWJ6jHrwY7ZyYSGRIIV-4MtgptHMVtcUpBbDTM9xHaBKpYAOsVetnjrfwiraSbGqt_r8NyVb_hpo2mfwqNWe3QRXBhndQeo8NBqe_XhYxd_Eo_HqoTsLnbWJBOrgWOYIA74n-mYA2OWht8g4lrtq0zSmd3e_8AfGBtGtSVfd49OC8yDCg6ktRdWrJb_V5o_hZCiyoTdKTelgshS1TLi8PErG4oInvxxIJGzb6XkePM7aSfbNdiWjDfIXlDkZGxMl2v-HQE2DKpbSAh4ylceM-MvIi0t-TCDtWUi-xZXC6aXujDSnN280EwCkgyVL8w45zQYujz7dYrjeImsIPHQ',
        'e': 'AQAB'
      }
    ]
  },
  'customQueryParams': null,
  'timeoutFactor': 0.75,
  'sessionCheckIntervall': 3000,
  'sessionCheckIFrameName': 'angular-oauth-oidc-check-session-iframe',
  'disableAtHashCheck': false,
  'skipSubjectCheck': false
}
